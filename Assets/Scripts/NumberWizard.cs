﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NumberWizard : MonoBehaviour
{
    [SerializeField] Text guessTextComponent;

    [SerializeField] int max;
    [SerializeField] int min;
    int guess;

    // Start is called before the first frame update
    void Start()
    {
        NextGuess();
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void OnPressHigher()
    {
        min = guess + 1;
        NextGuess();
    }

    public void OnPressLower()
    {
        max = guess - 1;
        NextGuess();
    }

    void NextGuess()
    {
        guess = Random.Range(min, max + 1);
        guessTextComponent.text = guess.ToString();
    }
}
